# Testing
coverage==7.4.0                  # Test coverage
ruff==0.1.13                     # Python linter

# Types
mypy==1.8.0                      # Static typing
